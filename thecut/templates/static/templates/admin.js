django.jQuery(document).ready(function($) {
    
    $('input[type="radio"].template_radio_select').removeClass(
        'template_radio_select').closest('ul').addClass(
        'template_radio_select js-enabled');
    
    $('.template_radio_select input').click(function() {
        $('.template_radio_select input').closest('li').removeClass('checked');
        $(this).closest('li').addClass('checked');
    });
    
    $('.template_radio_select input[checked="checked"]').closest(
        'li').addClass('checked');
    
    // IE bug means images inside labels don't cause input click event
    $('.template_radio_select label img').click(function(event) {
        var id = $(this).closest('label').attr('for');
        $('#' + id).click();
        event.preventDefault();
    });
    
});
