# -*- coding: utf-8 -*-
from django.forms import RadioSelect


class TemplateRadioSelect(RadioSelect):
    def __init__(self, *args, **kwargs):
        super(TemplateRadioSelect, self).__init__(*args, **kwargs)
        attrs = kwargs.pop('attrs', {})
        css_classes = attrs.get('class', '').split(' ')
        css_classes += ['template_radio_select']
        self.attrs.update({'class': ' '.join(css_classes)})
    
    class Media:
        css = {'all': ['templates/admin.css']}
        js = ['templates/admin.js']

