# -*- coding: utf-8 -*-
from django.contrib import admin
from thecut.templates.models import SelectableTemplate


class SelectableTemplateAdmin(admin.ModelAdmin):
    list_display = ['name', 'template_name', 'content_type', 'admin_thumbnail']
    list_filter = ['content_type']
    search_fields = ['name', 'template_name']
    
    def admin_thumbnail(self, obj):
        return u'<img src="%s" alt="%s" style="height: 132px; ' \
            'width: 212px;" />' %(obj.image.url, obj.name)
    admin_thumbnail.short_description = 'Preview'
    admin_thumbnail.allow_tags = True


admin.site.register(SelectableTemplate, SelectableTemplateAdmin)

