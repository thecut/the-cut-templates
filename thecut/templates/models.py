# -*- coding: utf-8 -*-
from django.contrib.contenttypes.models import ContentType
from django.db import models


class SelectableTemplate(models.Model):
    """Associate a template with a content type."""
    name = models.CharField(max_length=100)
    template_name = models.CharField(max_length=100)
    content_type = models.ForeignKey(ContentType)
    image = models.ImageField(upload_to='uploads/templates/images')
    is_enabled = models.BooleanField(default=True)
    
    class Meta:
        ordering = ['name']
    
    def __unicode__(self):
        return self.name

