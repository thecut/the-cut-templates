# -*- coding: utf-8 -*-
from django.forms import ChoiceField
from django.utils.safestring import mark_safe
from thecut.templates.models import SelectableTemplate
from thecut.templates.widgets import TemplateRadioSelect


class TemplateChoiceField(ChoiceField):
    """Template choice using radio select."""
    widget = TemplateRadioSelect
    
    def __init__(self, content_type, *args, **kwargs):
        super(TemplateChoiceField, self).__init__(*args, **kwargs)
        self.set_template_choices(content_type)
    
    def set_template_choices(self, content_type):
        choices = not self.required and [('', 'Default')] or []
        templates = SelectableTemplate.objects.filter(is_enabled=True,
            content_type=content_type)
        for template in templates:
            img_tag = mark_safe('<img src="%s" alt="%s" title="%s" ' \
                'style="height: 132px; width: 212px;" />' \
                %(template.image.url, template.name, template.name))
            choices += [(template.template_name, img_tag)]
        self.choices = choices

